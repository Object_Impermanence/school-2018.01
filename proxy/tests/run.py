from .server import ClientInterface

import argparse
import json
import Pyro4


def main():
    parser = argparse.ArgumentParser(description="Proxy server")
    parser.add_argument("--config", help="Path to config.json file", default="config.json")

    args = parser.parse_args()

    with open(args.config) as config_file:
        config = json.load(config_file)

    Pyro4.config.SERIALIZER = config["serializer"]
    Pyro4.config.SERIALIZERS_ACCEPTED.add(config["serializer"])

    daemon = Pyro4.Daemon(host=config["host"], port=config["port"])

    master_uri = config["master_uri"]
    client_server = ClientInterface(master_uri)
    daemon.register(client_server, "client")

    daemon.requestLoop()


if __name__ == "__main__":
    main()
